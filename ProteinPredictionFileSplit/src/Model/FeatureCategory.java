package Model;

import java.util.ArrayList;

public class FeatureCategory {
	public float numberOfFeaturesInCategory=0;
	
	public int numberOfPositiveFeatures=0;
	public int numberOfNegativeFeatures=0;
	
	//to be calculated after all data is available
	public float percentageOfPositiveFeatures;
	public float percentageOfNegativeFeatures;
	
	public float totalPercentageOfFeatureOverAllFeatures;
	public float percentageOfNegativeFeaturesOverAllFeatures;
	public float percentageOfPositiveFeaturesOverAllFeatures;
	
	public ArrayList<SingleFeature> arrayOfFeatures;
	
	public void addSingleFeatureToCategory(SingleFeature feature){
		this.arrayOfFeatures.add(feature);
		this.numberOfFeaturesInCategory+=1;
		if (feature.featureIsPlus) {
			numberOfPositiveFeatures+=1;
		}else {
			numberOfNegativeFeatures+=1;
		}
	}
	
	//should only be called after all of the data is filled
	public void calculatePercentagesForCategory(int totalNumberOfFeatures){
		//validate number of features
		if (this.numberOfFeaturesInCategory!=this.arrayOfFeatures.size()) {
			System.out.println("Error while validating array size");
		}
				
		this.totalPercentageOfFeatureOverAllFeatures = (100*this.numberOfFeaturesInCategory/totalNumberOfFeatures);
		
		this.percentageOfPositiveFeatures=this.numberOfPositiveFeatures*100/numberOfFeaturesInCategory;
		this.percentageOfPositiveFeaturesOverAllFeatures=this.totalPercentageOfFeatureOverAllFeatures*percentageOfPositiveFeatures/100;
		
		this.percentageOfNegativeFeatures=numberOfNegativeFeatures*100/numberOfFeaturesInCategory;
		this.percentageOfNegativeFeaturesOverAllFeatures=this.totalPercentageOfFeatureOverAllFeatures*percentageOfNegativeFeatures/100;
	}
	
}
