package Model;

import java.util.ArrayList;

import weka.classifiers.Evaluation;

public class ResultEntry {

	public ArrayList<String> arrayOfAttributes;
	public Evaluation evaluation;
	
	public double correct;
	public double incorrect;
	
	public double tp;
	public double tn;

	public double fp;
	public double fn;
	
	public double q2;
	public double acc_tm;
	public double cov_tm; 
	public double acc_non_tm;
	public double cov_non_tm;
	
	public void printInfo() throws Exception{
		System.out.println(evaluation.toClassDetailsString());
		
		double total = evaluation.correct() + evaluation.incorrect();
		System.out.println("Correct: " + evaluation.correct() + "         "
				+ (100 * evaluation.correct() / total) + " %");
		System.out.println("Incorrect: " + evaluation.incorrect() + "         "
				+ (100 * evaluation.incorrect() / total) + " %");
				
		System.out.println("TP: " + tp);// true positives
		System.out.println("TN: " + tn);// true negatives

		System.out.println("FP: " + fp);// false positives
		System.out.println("FN: " + fn);// false negatives

		System.out.println("Q2=" + q2);
		System.out.println("ACC=" + acc_tm);
		System.out.println("cov=" + cov_tm);
		System.out.println("ACC(non)=" + acc_non_tm);
		System.out.println("cov=(non)" + cov_non_tm);

	}
	
	public void calculate(){
		this.q2 = 100 * ((tp + tn) / (tp + fp + tn + fn));
		this.acc_tm = 100 * tp / (tp + fp);
		this.cov_tm = 100 * tp / (tp + fn);
		this.acc_non_tm = 100 * tn / (tn + fn);
		this.cov_non_tm = 100 * tn / (tn + fn);
	}

}
