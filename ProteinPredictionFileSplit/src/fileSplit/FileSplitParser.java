package fileSplit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

import Model.FeatureCategory;
import Model.SingleFeature;

public class FileSplitParser {

	public HashMap<String, FeatureCategory> listOfProteins = new HashMap<String, FeatureCategory>();
	
	//used to place correct categories into correct files
	int hasNUmberOfFolders=0;
	float totalFeaturePercentage = 0;
	float totalPositivePercentage = 0;
	int numberOfTries=0;
	int maxNumberOfTries=100;
	
	//TODO: remove gloabal variable
	String fileNameToReadFromString;
	
	int logoutPercentageEverNumberOfLines = 500;
	
	private String getNameOfFileWithExt(String fileName) {
		return new File(fileName).getName();
	}
	
	//if windowsize = 0 ignore the windowsize filter (it should be an odd number)
	public ArrayList<String> getBegOfFileUntillString(String fileName, String untilStirng, String createdFileName) throws FileNotFoundException{
		File file = new File(fileName);
		
		Scanner input = new Scanner(file);
		ArrayList<String> headerArrayList = new ArrayList<String>();
		boolean didReachEnd=false;
		
		//get file name for replacement
		String originFileNameString = this.getNameOfFileWithExt(this.fileNameToReadFromString);
		String destinationFileNameString = this.getNameOfFileWithExt(createdFileName);
		
		while (input.hasNext()&&(!didReachEnd)) {
			//proccess line by line
			String nextLine = input.nextLine();
			if ((nextLine.startsWith(untilStirng))) {
				didReachEnd=true;
			}else if(nextLine.startsWith("%")){
				//ignore
		} else if(nextLine.contains(originFileNameString)){
			headerArrayList.add(nextLine.replace(originFileNameString, destinationFileNameString));
		}
			else {
				headerArrayList.add(nextLine);
		}
		}
		input.close();
		
		return headerArrayList;
	}
	
	public HashMap<String, FeatureCategory> parseFileToCreateHashmapOfCategories(String fileNameToParse, String fileToSaveLogTo) 
			throws FileNotFoundException,UnsupportedEncodingException {
				
		this.fileNameToReadFromString=fileNameToParse;
		File file = new File(fileNameToParse);
		
		long fileSizeInBytes = file.length();
		//cca.. feature line is 21152 - from observation	
		Scanner input = new Scanner(file);

		double i = 0;
		// the total number of lines are 21715 - for percentage calculation
		
		int parsingStatusLine = 0;
		int totalNumberOfFeatures = 0;

		while (input.hasNext()) {
			
			i += 1;
			parsingStatusLine += 1;
			if (parsingStatusLine > this.logoutPercentageEverNumberOfLines) {
				parsingStatusLine = 0;
				System.out.println("Approximate parsing percentage: "+ (i*21152 / fileSizeInBytes * 100) + "%");
			}
			//proccess line by line
			String nextLine = input.nextLine();
			if (	(nextLine.startsWith("\n"))
					||(nextLine.startsWith("@DATA"))
					||(nextLine.startsWith("@ATTRIBUTE"))
					||(nextLine.startsWith("@RELATION"))
					||(nextLine.startsWith("%"))
					||(nextLine.length()==0)) {
				// ignore do not contain features
			} else {
				SingleFeature proteinStructure = new SingleFeature();

				String fullNameString = nextLine.substring(0, nextLine.indexOf(","));
				int index = nthOccurrence(fullNameString, '_', 1);

				if (index > 0) {
					proteinStructure.featureName = fullNameString.substring(0, index);
					proteinStructure.featureText = nextLine;

					// feature is valid if it has a + or - at the end
					boolean isFeatureValid = false;
					if (nextLine.endsWith("+")) {
						proteinStructure.featureIsPlus = true;
						isFeatureValid = true;
					} else if (nextLine.endsWith("-")) {
						proteinStructure.featureIsPlus = false;
						isFeatureValid = true;
					}
					if (isFeatureValid) {
						totalNumberOfFeatures += 1;
						if (listOfProteins.containsKey(proteinStructure.featureName)) {
							//category exists
							FeatureCategory featureCategory = this.listOfProteins.get(proteinStructure.featureName);
							featureCategory.addSingleFeatureToCategory(proteinStructure);
						} else {
							//category does not exist - create new category 
							FeatureCategory featureCategory=new FeatureCategory();
							featureCategory.arrayOfFeatures = new ArrayList<SingleFeature>();
							featureCategory.addSingleFeatureToCategory(proteinStructure);
							this.listOfProteins.put(proteinStructure.featureName, featureCategory);
						}
					}else {
						System.out.println("Error on line:" + nextLine);
					}
				}else {
					System.out.println("Error on line:" + nextLine);
				}
			}
		}
		input.close();
		
		//process data
		float togetherPositiveSumForValidation=0;
		float togetherNegativeSumForValidation=0;
		
		// finished parsing
		System.out.println("Calculating data and Writing info to Output Log File");
		
		PrintWriter writer = new PrintWriter(fileToSaveLogTo, "UTF-8");

		writer.println("Total Number Of Features: " + totalNumberOfFeatures);
		writer.println("Feature List: " + this.listOfProteins.size());

		for (Entry<String, FeatureCategory> entry : listOfProteins.entrySet()) {
			FeatureCategory featureCategory=(FeatureCategory)entry.getValue();
			
			featureCategory.calculatePercentagesForCategory(totalNumberOfFeatures);
			
			togetherNegativeSumForValidation+=featureCategory.percentageOfNegativeFeaturesOverAllFeatures;
			togetherPositiveSumForValidation+=featureCategory.percentageOfPositiveFeaturesOverAllFeatures;
			
			writer.println(entry.getKey()
					+ " num of Feat: "
					+ featureCategory.numberOfFeaturesInCategory
					+ " pos: " +featureCategory.numberOfPositiveFeatures +" (" + featureCategory.percentageOfPositiveFeatures +"%) = " + featureCategory.percentageOfPositiveFeaturesOverAllFeatures
					+ " neg: " +featureCategory.numberOfNegativeFeatures +" (" + featureCategory.percentageOfNegativeFeatures +"%) = " + featureCategory.percentageOfNegativeFeaturesOverAllFeatures
					+ " Total % in dataset: " + featureCategory.totalPercentageOfFeatureOverAllFeatures+"%");
		}
		writer.println("  Total positive   " + togetherPositiveSumForValidation);
		writer.println("  Total neg   " + togetherNegativeSumForValidation);
		writer.println("  Total validation   " + (togetherNegativeSumForValidation+togetherPositiveSumForValidation));
		writer.close();
		
		return this.listOfProteins;
	}


	public void generateFilesFromHashMap(String fileBaseName) throws FileNotFoundException,UnsupportedEncodingException {
		
		HashMap<String, FeatureCategory> folder1 = new HashMap<String, FeatureCategory>();
		HashMap<String, FeatureCategory> folder2 = new HashMap<String, FeatureCategory>();
		HashMap<String, FeatureCategory> folder3 = new HashMap<String, FeatureCategory>();
		
		//create 3 different folders
		
		while ((hasNUmberOfFolders<3)
				&&(this.listOfProteins.size()>0)) {
			
			numberOfTries+=1;
			if (numberOfTries>maxNumberOfTries) {
				System.out.println("Trying again!!");
				this.returnElementsToHashMap(folder1);
				this.returnElementsToHashMap(folder2);
				this.returnElementsToHashMap(folder3);
				numberOfTries=0;
				totalFeaturePercentage = 0;
				totalPositivePercentage = 0;
			}else {
				Random       random    = new Random();
				List<String> keys      = new ArrayList<String>(listOfProteins.keySet());
				String       randomKey = keys.get( random.nextInt(keys.size()));			
				FeatureCategory featureCategory = listOfProteins.get(randomKey);
				
				this.listOfProteins.remove(randomKey);

				if (hasNUmberOfFolders==0) {
					this.addMapEntryToFolderAndCheckPercentage(randomKey, featureCategory, folder1);
				}else if (hasNUmberOfFolders==1) {
					this.addMapEntryToFolderAndCheckPercentage(randomKey, featureCategory, folder2);
				}else if (hasNUmberOfFolders==2) {
					this.addMapEntryToFolderAndCheckPercentage(randomKey, featureCategory, folder3);
				}
			}
		}
		
		this.printHashMapToFile(folder1, fileBaseName +"1.arff");
		this.printHashMapToFile(folder2, fileBaseName +"2.arff");
		this.printHashMapToFile(folder3, fileBaseName +"3.arff");
		
	}
	
	private void addMapEntryToFolderAndCheckPercentage(String key, FeatureCategory featureCategory, HashMap<String, FeatureCategory> folder) {
		folder.put(key, featureCategory);
		totalFeaturePercentage+=featureCategory.totalPercentageOfFeatureOverAllFeatures;
		totalPositivePercentage+=featureCategory.percentageOfPositiveFeaturesOverAllFeatures;
		//33.33333
//		System.out.println(totalFeaturePercentage);

		if (totalFeaturePercentage>33.33&&totalFeaturePercentage<33.34) {
			//10.3159
			if (totalPositivePercentage>10.31&&totalPositivePercentage<10.32) {
				System.out.println("Feature %" + totalFeaturePercentage + " Positive %" +totalPositivePercentage);
				numberOfTries=0;
				totalFeaturePercentage = 0;
				totalPositivePercentage = 0;
				hasNUmberOfFolders+=1;
			}else {
				this.returnElementsToHashMap(folder);
				numberOfTries=0;
				totalFeaturePercentage = 0;
				totalPositivePercentage = 0;
			}
		}else if (totalFeaturePercentage>=33.5) {
			this.returnElementsToHashMap(folder);
			numberOfTries=0;
			totalFeaturePercentage = 0;
			totalPositivePercentage = 0;
		}
		else if (totalFeaturePercentage<=33.33) {
			//do nothing.. let the last folder exit if it doesnt meet the exact requirements
		}
	}
	
	private void printHashMapToFile(HashMap<String, FeatureCategory> hashMap, String fileName) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		writer.println("%Created by Protein Prediction Group 7 Auto-Generator");
		
		//hard coded
		ArrayList< String> headerArray = this.getBegOfFileUntillString(this.fileNameToReadFromString, "@DATA", fileName);
		for (String s : headerArray){
			writer.println(s);
		}
		
		writer.println("@DATA");
		for (Entry<String, FeatureCategory> entry : hashMap.entrySet()) {
			FeatureCategory featureCategory=(FeatureCategory)entry.getValue();
			for (SingleFeature s : featureCategory.arrayOfFeatures){
				writer.println(s.featureText);
			}
		}		
		writer.close();
		
	}
	
	private void returnElementsToHashMap(HashMap<String, FeatureCategory> hashMap){
		
		for(Iterator<Map.Entry<String, FeatureCategory>> it = hashMap.entrySet().iterator(); it.hasNext(); ) {
		      Map.Entry<String, FeatureCategory> entry = it.next();
		      	String key=entry.getKey();
				FeatureCategory featureCategory=(FeatureCategory)entry.getValue();
				it.remove();
				
				this.listOfProteins.put(key, featureCategory);
		    }
	}

	public static int nthOccurrence(String str, char c, int n) {
		int pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos + 1);
		return pos;
	}
	
	
}



//for (int i = num; i >= 0; i--) {
////System.out.println(((Attribute)data.attribute(i)));
//String attributeString=data.attribute(i).toString();
//if (!attributeString.contains("pssm")&&!attributeString.contains("class")) {
//	data.deleteAttributeAt(i);
//}
//}
//num = data.numAttributes()-1;
//System.out.println("Total number of attributes is:"+num);
