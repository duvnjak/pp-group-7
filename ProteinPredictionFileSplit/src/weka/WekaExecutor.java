package weka;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import Model.AttributePropertiesInFile;
import Model.ResultEntry;

public class WekaExecutor {

	public int totalNumberOfAttributes = 0;
	public boolean includeLastAttribute = false;
	public int windowSize = 0;
	public ArrayList<ResultEntry> arrayOfResults=new ArrayList<ResultEntry>();
	
	public HashMap<String, AttributePropertiesInFile> attributePositions;
	public Instances wholeDataSetInstances;

	public Instances generateDataModelFromFile(String fileName) throws Exception {
		System.out.println("Weka API analyzing data and setting dataset form file...");
		DataSource source = new DataSource(fileName);
		Instances wholeDataset = source.getDataSet();
		// setting class attribute if the data format does not provide this information
		// For example, the XRFF format saves the class attribute information as well
		if (wholeDataset.classIndex() == -1) {
			wholeDataset.setClassIndex(wholeDataset.numAttributes() - 1);
		}
		System.out.println("Dataset generated!");
		System.out.println("Total number of attributes is:" + (wholeDataset.numAttributes()));
		return wholeDataset;
	}
	
	public Instances generateNewDatasetWithFilteredItems(Instances wholeDataset,
			HashMap<String, AttributePropertiesInFile> attPositionHashMap,
			ArrayList<String> includeAttArrayList) throws Exception{

		System.out.println("*****************************************************");
		
		// generate remove parameters from arraylist
				ArrayList<AttributePropertiesInFile> intervalArrayList = new ArrayList<AttributePropertiesInFile>();

				for (String s : includeAttArrayList) {
					if (attPositionHashMap.containsKey(s)) {
						AttributePropertiesInFile attPosition = attPositionHashMap.get(s);
						intervalArrayList.add(attPosition);
					} else {
						System.out.println("Wrong Attribute Name: " + s);
					}
				}

				//generate this -R interval
				String requestString = this.generateInterval(intervalArrayList);

				String[] options2 = new String[2];
				options2[0] = "-R";
				// options2[1] = "1-1,582-2885";
				options2[1] = requestString;

				Remove remove = new Remove(); // apply filter
				remove.setOptions(options2);
				remove.setInputFormat(wholeDataset); 
				Instances newData = Filter.useFilter(wholeDataset, remove);

		return newData;
	}
	
	
	public ResultEntry executeRandomForestWithDataSet(Instances dataSet, ArrayList<String> includeAttArrayList) throws Exception{
		System.out.println("Processing request for attributes:");
		for (String string : includeAttArrayList) {
			System.out.println("      "+ string);
		}
		
		System.out.println("Setting up random forest parameters!");
		RandomForest scheme = new RandomForest();
		scheme.setMaxDepth(0);
		scheme.setNumTrees(10);
		scheme.setSeed(1);
		scheme.setMaxDepth(0);

		Evaluation eval = new Evaluation(dataSet);
		Random rand = new Random(1); // using seed = 1
		int folds = 10;

		System.out.println("Running the Cross Validation");
		eval.crossValidateModel(scheme, dataSet, folds, rand);

		ResultEntry resultEntry=new ResultEntry();
		resultEntry.arrayOfAttributes=includeAttArrayList;
		resultEntry.evaluation=eval;
		resultEntry.correct = eval.correct();
		resultEntry.incorrect = eval.incorrect();
		
		resultEntry.tp = eval.confusionMatrix()[0][0];
		resultEntry.tn = eval.confusionMatrix()[1][1];
		
		resultEntry.fp = eval.confusionMatrix()[1][0];
		resultEntry.fn = eval.confusionMatrix()[0][1];
		resultEntry.incorrect = eval.incorrect();
		resultEntry.incorrect = eval.incorrect();
		
		return resultEntry;
	}
	

	// weka API
	// just testing the weka interface
//	public void executeWeka(
//			Instances wholeDataset,
//			HashMap<String, AttributePropertiesInFile> attPositionHashMap,
//			ArrayList<String> includeAttArrayList) throws Exception {
//
//		Instances newData= this.generateNewDatasetWithFilteredItems(wholeDataset, attPositionHashMap, includeAttArrayList);
//
//	}
	
	private String generateInterval(ArrayList<AttributePropertiesInFile> intervalArrayList){
		// generate interval - tricky because we need to set the opposite
				// interval from the ones chosen
				String requestString = "";
				int previousHighBounds = 0;
				while (intervalArrayList.size() > 0) {
					int lowBounds = 50000;
					int highBounds = 0;
					AttributePropertiesInFile attToRemove = null;
					for (AttributePropertiesInFile att : intervalArrayList) {
						if (att.startPosition < lowBounds) {
							lowBounds = att.startPosition;
							// start position is the one after - compared to weka UI
							highBounds = att.endPosition + 1;
							attToRemove = att;
							// System.out.println(lowBounds+" : "+highBounds);
						}
					}
					if (requestString.equalsIgnoreCase("")) {
						// first entry
						requestString = "1-" + lowBounds + "," + highBounds;
					} else {
						if (previousHighBounds >= lowBounds) {
							// the previous entry and this one are one after the other
							// delete last highbouds and enter your highbounds
							// (otherwise an entry would be 78-78 which still deletes
							// but shouldnt)
							requestString = requestString.substring(0,
									requestString.lastIndexOf(",") + 1);
							requestString = requestString + highBounds;
						} else {
							requestString = requestString + "-" + lowBounds + ","
									+ highBounds;
						}

					}
					previousHighBounds = highBounds;
					intervalArrayList.remove(attToRemove);
				}
				// add total number (do not delete the last instance - "class")
				if (includeLastAttribute) {
					requestString = requestString + "-"	+ (this.totalNumberOfAttributes - 1);
				} else {
					requestString = requestString + "-"	+ (this.totalNumberOfAttributes);
				}
				System.out.println("Request to remove certain fetures from dataset: " + requestString);
				System.out.println("Use this info to double check with the UI tool: " + requestString);
				return requestString;
	}

	// analyses the file and return a hashmap with attribute names as key and
	// their start and end position (the attributes must be sorted by type for
	// this to work)
	// should be used for fast deletion for the weka API
	public HashMap<String, AttributePropertiesInFile> analyzeAttributesForFile(
			String fileName) throws FileNotFoundException {

		System.out.println("Analyzing attributes from file!");
		System.out.println("Attribute families in file:");

		File file = new File(fileName);
		Scanner input = new Scanner(file);

		int lineNumber = 0;
		HashMap<String, AttributePropertiesInFile> attributeProperties = new HashMap<String, AttributePropertiesInFile>();
		String currentAttributeNameString = "";
		int currentAttributeStartNumber = 0;
		boolean isHeader = true;

		while (input.hasNext() && isHeader) {

			String nextLine = input.nextLine();
			if (nextLine.startsWith("@ATTRIBUTE")) {

				// all attributes either have one or two "_" and the name is
				// always after the last one
				// 10_I_pssm -14_T_perc 10_infPP -14_disis_plus 6_disis_raw
				// 3_isis_plus 10_md_minus
				// 9_md_ri -9_helix -1_e 5_chemprop_vol 7_chemprop_hbreaker
				int startIndex = nthOccurrence(nextLine, '_', 1);
				if (startIndex < 0) {
					startIndex = nthOccurrence(nextLine, '_', 0);
				} else {
					// 14_T_perc 7_chemprop_hbreaker
					if (nthOccurrence(nextLine, '_', 1) == ((nthOccurrence(
							nextLine, '_', 0)) + 2)) {
						startIndex = nthOccurrence(nextLine, '_', 1);
					} else {
						startIndex = nthOccurrence(nextLine, '_', 0);
					}
				}

				int endIndex = nthOccurrence(nextLine, ' ', 1);
				if (startIndex > 0 && endIndex > 0) {
					String attributeString = nextLine.substring(startIndex + 1,
							endIndex);
					if (currentAttributeNameString
							.equalsIgnoreCase(attributeString)) {
						// still same feature..continue
					} else {
						if (!(currentAttributeNameString.equals(""))) {
							// do not store on inital start of loop
							System.out.println("         "
									+ currentAttributeNameString);
							AttributePropertiesInFile attribute = new AttributePropertiesInFile();
							attribute.endPosition = lineNumber;
							attribute.startPosition = currentAttributeStartNumber;
							attributeProperties.put(currentAttributeNameString,
									attribute);
						}
						currentAttributeStartNumber = lineNumber;
						currentAttributeNameString = attributeString;
					}
				}
				lineNumber += 1;
			} else if (nextLine.startsWith("@DATA")) {
				isHeader = false;
			}
		}

		this.totalNumberOfAttributes = lineNumber;
		input.close();

		// AttributePropertiesInFile
		// attributePropertiesInFile=attributeProperties.get("strand");
		// System.out.println(attributePropertiesInFile.startPosition);
		// System.out.println(attributePropertiesInFile.endPosition);
		return attributeProperties;
	}

	// very slow operation - perform on the smallest possible dataset
	public Instances setWindowSizeOnDataSet(Instances dataset, int windowSize) {
		System.out
				.println("Setting window size on dataset:" + windowSize);
		System.out
		.println("This function is extremely slow.. call it on the smallest possible list!");
		if (windowSize == 0) {
			System.out.println("Attributes remaining in dataset:" + (dataset.numAttributes()));
			return dataset;
		}
		int trueWindowSize = (windowSize - 1) / 2;
		for (int i = dataset.numAttributes() - 1; i >= 0; i--) {
			String attributeString = dataset.attribute(i).toString();
			if (attributeString.indexOf("_") > 0) {
				attributeString = attributeString.substring(11,
						attributeString.indexOf("_"));
				 try { 
					 int value = Integer.valueOf(attributeString);
						if ((value > trueWindowSize) | (value < (-1 * trueWindowSize))) {
							dataset.deleteAttributeAt(i);
							// System.out.println("deleting "+attributeString);
						} 
				    } catch(NumberFormatException e) { 
				       //ignore attribute - do not delete
//				    	System.out.println("Atribute does not contain number:" + attributeString);
				    }
			}else {
//				System.out.println("Atribute does not contain number:" + attributeString);
			}
		}
		System.out.println("Attributes remaining in dataset:" + (dataset.numAttributes()));
		return null;
	}

	public static int nthOccurrence(String str, char c, int n) {
		int pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos + 1);
		return pos;
	}

	public void bruteForseIterate(ArrayList<String> obligatoryAtts,
			ArrayList<String> nonObligatoryAtts) throws Exception {
		System.out.println("Number of iterations: 2^"
				+ nonObligatoryAtts.size());

		ArrayList<String> allCombinations = new ArrayList<String>();
		printArrayPermutations(allCombinations, nonObligatoryAtts, 0,obligatoryAtts);

	}

	public void printArrayPermutations(ArrayList<String> n,
			ArrayList<String> nonObligatoryAtts, int idx,
			ArrayList<String> obligatoryAtts) throws Exception {
		if (idx == nonObligatoryAtts.size()) { // stop condition for the
												// recursion [base clause]
			ArrayList<String>invocationArrayList=new ArrayList<String>();
			
			for (String obl : obligatoryAtts) {
				invocationArrayList.add(obl);
			}
			for (String s : n) {
				invocationArrayList.add(s);
			}
			//call function here with invocationArray
			
			Instances newData= this.generateNewDatasetWithFilteredItems(wholeDataSetInstances, attributePositions, invocationArrayList);
			this.setWindowSizeOnDataSet(newData, 15);
			ResultEntry resultEntry = this.executeRandomForestWithDataSet(newData, invocationArrayList);
			//use this for comparison
//			this.arrayOfResults.add(resultEntry);
			resultEntry.calculate();
			resultEntry.printInfo();
		
			return;
		}
		for (int i = 0; i <= 1; i++) {
			if (i == 0) {
				printArrayPermutations(n, nonObligatoryAtts, idx + 1,
						obligatoryAtts); // recursive invokation, for next
											// elements
			} else {
				n.add(nonObligatoryAtts.get(idx));
				printArrayPermutations(n, nonObligatoryAtts, idx + 1,
						obligatoryAtts); // recursive invokation, for next
											// elements
				n.remove(n.size() - 1);
			}
		}
	}

}
