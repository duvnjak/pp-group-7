package weka;

import java.util.ArrayList;
import java.util.HashMap;

import weka.core.Instances;
import Model.AttributePropertiesInFile;

public class WekaExecutorMain {
	
	public static void main(String[] args) {
		WekaExecutor wekaExecutor=new WekaExecutor();
		try {
			
			ArrayList<String> obligatoryAtts = new ArrayList<String>();
			obligatoryAtts.add("pssm");
			obligatoryAtts.add("chemprop_hyd");
			
			ArrayList<String> nonObligatoryAtts = new ArrayList<String>();
			nonObligatoryAtts.add("perc");
			nonObligatoryAtts.add("loop");

			String fileName = "resources/file1.arff";
			HashMap<String, AttributePropertiesInFile> attributePositions = wekaExecutor.analyzeAttributesForFile(fileName);
			Instances wholeDataSet = wekaExecutor.generateDataModelFromFile(fileName);
			//this is very slow so use it on the smallest set possible or use it only once
			System.out.println("Attributes remaining in dataset:" + (wholeDataSet.numAttributes()));
			wekaExecutor.attributePositions=attributePositions;
			wekaExecutor.wholeDataSetInstances=wholeDataSet;
			wekaExecutor.includeLastAttribute=true;
			wekaExecutor.bruteForseIterate(obligatoryAtts, nonObligatoryAtts);
			
			
//			String fileName = "resources/file1.arff";
//			HashMap<String, AttributePropertiesInFile> attributePositions = wekaExecutor.analyzeAttributesForFile(fileName);
//			Instances wholeDataSet = wekaExecutor.generateDataModelFromFile(fileName);
//			
//			//set up data model
//			ArrayList<String> includeArrayList = new ArrayList<String>();
//			includeArrayList.add("pssm");
//			includeArrayList.add("perc");
//			includeArrayList.add("loop");
//			//include the class attribute
//			wekaExecutor.includeLastAttribute=true;
//			
//			Instances newData= wekaExecutor.generateNewDatasetWithFilteredItems(wholeDataSet, attributePositions, includeArrayList);
//			wekaExecutor.setWindowSizeOnDataSet(newData, 15);
//			ResultEntry resultEntry = wekaExecutor.executeRandomForestWithDataSet(newData, includeArrayList);
//			resultEntry.calculate();
//			resultEntry.printInfo();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

}
